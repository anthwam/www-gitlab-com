### Tech Stack Update

**Please do not merge this MR before the Business Systems Analysts have reviewed and approved.**

Please take into account:

* If changing provisioners: You must also open an [Update Tech Stack provisioner issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Update_Tech_Stack_Provisioner) so all templates and Tech Stack information can be updated.
* If completing a system offboarding:
   * Follow the steps in the [Tech Stack offboarding section](/handbook/business-ops/tech-stack-applications/#tech-stack-offboarding)

/cc @lisvinueza 
/label ~"BusinessTechnology" ~"BT - TechStack" ~"BT-TechStack::To do" 

