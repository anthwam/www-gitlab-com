---
layout: handbook-page-toc
title: "Strategies for Non-Engaged Customers"
---

Gainsight contains a playbook for non-engaged customers that *will* trigger after a customer has had no engagement for greater than 60 days and is a Priority 1 customer.  Until this rule is activated, this playbook is available to be manually opened, in the 'Risk' CTA Type

The playbook contains the following steps:

## Playbook

1. Sync with SAL/AE on a reach-out strategy
   - TAM to day to day contacts
   - SAL to buyers /influencers/ anyone else they have relationships with
   - SDR to any contacts outside of our relationship
1. Create a compelling message (s) : Review objectives of the customer and fashion messaging for ‘why’ the customer should engage with us.  Consider the following:
   1. We know they are migrating from another platform, so offer training & enablement
   1. We know they are interested in xxx so reach out offering to discuss new feature yyy
   1. Review support tickets and build on any conversation that is happening to offer value
1. Create Triage Issue If no engagement in step 2
   1. Escalate via the triage issue: At this point the tracking of engagement strategies will live in the Triage Issue. In this issue, loop in a more senior GitLab resource (decide level based on size of customer with TAM Manager) to reach out to someone on the customer side if still no response after 1 month.  Craft the email and provide any context on business objectives available to the GitLab senior resource.

## Additional strategies to consider

### List of sources to review

- SFDC activity history -> find any email responses from the customer about their projects or technical needs, and reply to those contacts asking if they were able to implement, and if they have questions.
- Zendesk tickets
- LinkedIn prospecting (best w/ Sales Navigator). Need to describe this process, how to find the right roles, and best practices for contacting them.
- Any known GitLab usernames, check for activity on issues. Try to follow-up to see if they implemented or found workarounds.
- Google docs meeting notes - look for any Positive Business Outcomes (PBOs) or Required Capabilities, or tech stack mentions.
- Previous Chorus meetings, find people and reasons to inquire if they were successful, ask for a meeting to see how we can help.
- SDRs to help prospect, and alert SAL/AE/SA/TAM on meetings or responses. TAM to give SDR pointers on relevant topics.
- If different from who we normally work with, contact economic buyer from license app or SFDC
- If large enough, ask exec group and board for networking/introductions
- Perform a news search for any major events such as acquisitions, layoffs, product launches

### Additional Items

- Reach out at different times of day and days of the week
- Continue to reach out in staggered manner every 1-2 weeks
- Highlight what benefit they'll get out of the call (make it focused on them, not me)
- Show the value of a TAM, e.g. share new GitLab functionalities that might interest them and that we can share more relevant features once we know more about how they use GitLab
- Have something actionable and specific so they don't have to do as much work (e.g. instead of "when are you free for a call?", ask "are you available Thursday at 2?")
- Just schedule a call and see if they show up (works more often than not 🤷)
