---
layout: handbook-page-toc
title: "SOX Internal Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Desktop Procedures

[Guidance on updating the Desktop Procedures](/handbook/finance/sox-internal-controls/guidance-on-updating-the-desktop-procedures/)

1. [Quote to Cash](/handbook/finance/sox-internal-controls/quote-to-cash/) 
1. [Procure to Pay](/handbook/finance/sox-internal-controls/procure-to-pay/)
1. [Hire to Retire](/handbook/finance/sox-internal-controls/hire-to-retire/)
1. [Regulatory](/handbook/finance/sox-internal-controls/regulatory/)
1. [Record to Report](/handbook/finance/sox-internal-controls/record-to-report/)
1. [Stock Compensation](/handbook/finance/sox-internal-controls/stock-compensation/)

### Entity Level Controls
[Guidance on Entity Level Controls](/handbook/finance/sox-internal-controls/entity-level-controls/)


### IT General Controls
[Guidance on IT General Controls](/handbook/engineering/security/security-assurance/security-compliance/ITGC/)


### SOX Certifications 
[Guidance on SOX Certifications](/handbook/finance/sox-internal-controls/sox-certifications/)
